package com.mobsandgeeks.mylibrary;

import commons.validator.routines.DomainValidator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class DomainTest {

    private DomainValidator validator;

    @Before
    public void init() {
        validator = DomainValidator.getInstance();
    }

    @Test
    public void domainTest() {
        Assert.assertTrue(validator.isValid("cccc.aaa.com"));
        Assert.assertTrue(validator.isValid("aaa.cc"));
        Assert.assertFalse(validator.isValid("aaa.cc/aa"));
    }
}
