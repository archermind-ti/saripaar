package com.mobsandgeeks.mylibrary;

import commons.validator.routines.UrlValidator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class UrlTest {

    private UrlValidator validator;

    @Before
    public void init() {
        validator = UrlValidator.getInstance();
    }

    @Test
    public void urlTest() {
        Assert.assertTrue(validator.isValid("https://aaa.ccc.com"));
        Assert.assertTrue(validator.isValid("http://aaa.ccc.com"));
        Assert.assertTrue(validator.isValid("https://aaa.ccc.com/fea"));
    }
}
