package com.mobsandgeeks.mylibrary;

import commons.validator.routines.EmailValidator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class EmailTest {

    private EmailValidator validator;

    @Before
    public void init() {
        validator = EmailValidator.getInstance();
    }

    @Test
    public void emailTest() {
        Assert.assertFalse(validator.isValid("feafeaw@feafewa"));
        Assert.assertTrue(validator.isValid("feafeaw@feafewa.cc"));
        Assert.assertTrue(validator.isValid("www.feafeaw@feafewa.com"));
    }
}
