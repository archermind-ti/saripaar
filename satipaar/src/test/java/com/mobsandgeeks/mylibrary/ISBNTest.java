package com.mobsandgeeks.mylibrary;

import commons.validator.routines.ISBNValidator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ISBNTest {

    private ISBNValidator validator;

    @Before
    public void init() {
        validator = ISBNValidator.getInstance();
    }

    @Test
    public void isbnTest() {
        Assert.assertTrue(validator.isValid("0-670-82162-4"));
        Assert.assertTrue(validator.isValid("978-7-5170-5774-1"));
    }
}
