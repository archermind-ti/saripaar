package com.mobsandgeeks.mylibrary;

import commons.validator.routines.InetAddressValidator;
import org.junit.Assert;
import org.junit.Test;

public class InetAddressTest {

    @Test
    public void ipTest() {
        Assert.assertTrue(InetAddressValidator.getInstance().isValid("127.0.0.1"));
        Assert.assertTrue(InetAddressValidator.getInstance().isValid("0.0.0.0"));
        Assert.assertTrue(InetAddressValidator.getInstance().isValid("::1"));
    }
}
