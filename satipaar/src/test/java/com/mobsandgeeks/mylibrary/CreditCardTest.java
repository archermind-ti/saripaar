package com.mobsandgeeks.mylibrary;

import commons.validator.routines.CreditCardValidator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CreditCardTest {

    private CreditCardValidator validator;

    @Before
    public void init() {
        validator = new CreditCardValidator();
    }

    @Test
    public void masterCardTest() {
        Assert.assertTrue(validator.isValid("5105105105105100"));
    }

    @Test
    public void visaCardTest() {
        Assert.assertTrue(validator.isValid("4590613013277775"));
    }

    @Test
    public void amexCardTest() {
        Assert.assertTrue(validator.isValid("378282246310005"));
    }

    @Test
    public void discoverCardTest() {
        Assert.assertTrue(validator.isValid("6011111111111117"));
    }

    @Test
    public void dinnerCardTest() {
        CreditCardValidator dinnerCardValidator = new CreditCardValidator(CreditCardValidator.DINERS);
        Assert.assertTrue(dinnerCardValidator.isValid("38520000023237"));
    }
}
