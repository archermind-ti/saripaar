/**
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.mobsandgeeks.saripaar;

import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

public class HiLogUtils {

    public static final String TAG = "HiLogUtils";
    private static final int LOG_DOMAIN = 0x00201;
    private static final HiLogLabel LABEL = new HiLogLabel(3, 0xD001100, "Demo");
    public static final boolean flag = false;

    public static void debug(String msg) {
        if (!flag) {
            HiLog.debug(LABEL, msg);
        }
    }

    public static void error(String msg) {
        if (!flag) {
            HiLog.error(LABEL, msg);
        }
    }

    public static void warn(String msg) {
        if (!flag) {
            HiLog.warn(LABEL, msg);

        }
    }

    public static void info(String msg) {
        if (!flag) {
            HiLog.info(LABEL, msg);
        }
    }

    public static void fatal(String msg) {
        if (!flag) {
            HiLog.fatal(LABEL, msg);
        }
    }

    public static void info(String tag, String msg, Object... args) {
        if (!flag) {
            HiLog.info(new HiLogLabel(HiLog.LOG_APP, LOG_DOMAIN, tag), msg, args);
        }
    }

    public static void warn(String tag, String msg, Object... args) {
        if (!flag) {
            HiLog.warn(new HiLogLabel(HiLog.LOG_APP, LOG_DOMAIN, tag), msg, args);
        }
    }

    public static void error(String tag, String msg, Object... args) {
        if (!flag) {
            HiLog.error(new HiLogLabel(HiLog.LOG_APP, LOG_DOMAIN, tag), msg, args);
        }
    }
}
