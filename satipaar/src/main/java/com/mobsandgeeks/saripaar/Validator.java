/*
 * Copyright (C) 2014 Mobs & Geeks
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mobsandgeeks.saripaar;

import com.mobsandgeeks.saripaar.adapter.*;
import com.mobsandgeeks.saripaar.annotation.Optional;
import com.mobsandgeeks.saripaar.annotation.*;
import com.mobsandgeeks.saripaar.exception.ConversionException;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.agp.components.*;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.utils.Pair;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.*;

/**
 * The {@link Validator} takes care of validating the
 * {@link Component}s in the given controller instance. Usually, an
 * {@link AbilitySlice} or a {@link Fraction}. However, it can also be used
 * with other controller classes that contain references to {@link Component} objects.
 * <p>
 * The {@link Validator} is capable of performing validations in two
 * modes,
 *  <ol>
 *      <li>{@link Mode#BURST}, where all the views are validated and all errors are reported
 *          via the callback at once. Fields need not be ordered using the
 *          {@link Order} annotation in {@code BURST} mode.
 *      </li>
 *      <li>{@link Mode#IMMEDIATE}, in which the validation stops and the error is reported as soon
 *          as a {@link Rule} fails. To use this mode, the fields SHOULD
 *          BE ordered using the {@link Order} annotation.
 *      </li>
 *  </ol>
 * <p>
 * There are three flavors of the {@code validate()} method.
 * <ol>
 *      <li>{@link #validate()}, no frills regular validation that validates all
 *          {@link Component}s.
 *      </li>
 *      <li>{@link #validateTill(Component)}, validates all {@link Component}s till
 *          the one that is specified.
 *      </li>
 *      <li>{@link #validateBefore(Component)}, validates all {@link Component}s
 *          before the specified {@link Component}.
 *      </li>
 * </ol>
 * <p>
 * It is imperative that the fields are ordered while making the
 * {@link #validateTill(Component)} and {@link #validateBefore(Component)} method
 * calls.
 * <p>
 * The {@link Validator} requires a
 * {@link ValidationListener} that reports the outcome of the
 * validation.
 * <ul>
 *      <li> {@link ValidationListener#onValidationSucceeded()}
 *          is called if all {@link Rule}s pass.
 *      </li>
 *      <li>
 *          The {@link ValidationListener#onValidationFailed(List)}
 *          callback reports errors caused by failures. In {@link Mode#IMMEDIATE} this callback will
 *          contain just one instance of the {@link ValidationError}
 *          object.
 *      </li>
 * </ul>
 *
 * @author Ragunath Jawahar {@literal <rj@mobsandgeeks.com>}
 * @since 1.0
 */
@SuppressWarnings({"unchecked", "ForLoopReplaceableByForEach"})
public class Validator {

    // Entries are registered inside a static block (Placed at the end of source)
    private static final Registry SARIPAAR_REGISTRY = new Registry();

    // Holds adapter entries that are mapped to corresponding views.
    private final
    Map<Class<? extends Component>, HashMap<Class<?>, ViewDataAdapter>> mRegisteredAdaptersMap =
            new HashMap<Class<? extends Component>, HashMap<Class<?>, ViewDataAdapter>>();

    // Attributes
    private final Object mController;
    private Mode mValidationMode;
    private ValidationContext mValidationContext;
    private Map<Component, ArrayList<Pair<Rule, ViewDataAdapter>>> mViewRulesMap;
    private Map<Component, ArrayList<Pair<Annotation, ViewDataAdapter>>> mOptionalViewsMap;
    private boolean mOrderedFields;
    private boolean mValidateInvisibleViews;
    private final SequenceComparator mSequenceComparator;
    private ViewValidatedAction mViewValidatedAction;
    private EventHandler mViewValidatedActionHandler;
    private ValidationListener mValidationListener;
    private AsyncValidationTask mAsyncValidationTask;

    /**
     * Constructor.
     *
     * @param controller The class containing {@link Component}s to be validated. Usually,
     *                   an {@link Ability} or a {@link Fraction}.
     */
    public Validator(final Object controller) {
        assertNotNull(controller, "controller");
        mController = controller;
        mValidationMode = Mode.BURST;
        mSequenceComparator = new SequenceComparator();
        mViewValidatedAction = new DefaultViewValidatedAction();

        // Instantiate a ValidationContext
        if (controller instanceof Ability) {
            mValidationContext = new ValidationContext((Ability) controller);
        } else if (controller instanceof AbilitySlice) {
            mValidationContext = new ValidationContext(((AbilitySlice) controller).getAbility());
        } else if (controller instanceof Fraction) {
            Ability AbilitySlice = ((Fraction) controller).getFractionAbility();
            mValidationContext = new ValidationContext(AbilitySlice);
        }
        // Else, lazy init ValidationContext in #getRuleAdapterPair(Annotation, Field)
        // or void #put(VIEW, QuickRule<VIEW>) by obtaining a Context from one of the
        // View instances.
    }

    /**
     * A convenience method for registering {@link Rule} annotations that
     * act on {@link Text} and it's children, the most notable one being
     * {@link TextField}. Register custom annotations for
     * {@link Text}s that validates {@link Double},
     * {@link Float}, {@link Integer} and {@link String} types.
     * <p>
     * For registering rule annotations for other view types see,
     * {@link #registerAdapter(Class, ViewDataAdapter)}.
     *
     * @param ruleAnnotation A rule {@link Annotation}.
     */
    public static void registerAnnotation(final Class<? extends Annotation> ruleAnnotation) {
        SARIPAAR_REGISTRY.register(ruleAnnotation);
    }

    /**
     * An elaborate method for registering custom rule annotations.
     *
     * @param annotation      The annotation that you want to register.
     * @param viewType        The {@link Component} type.
     * @param viewDataAdapter An instance of the
     *                        {@link ViewDataAdapter} for your
     *                        {@link Component}.
     * @param <VIEW>          The {@link Component} for which the
     *                        {@link Annotation} and
     *                        {@link ViewDataAdapter} is being registered.
     */
    public static <VIEW extends Component> void registerAnnotation(
            final Class<? extends Annotation> annotation, final Class<VIEW> viewType,
            final ViewDataAdapter<VIEW, ?> viewDataAdapter) {

        ValidateUsing validateUsing = annotation.getAnnotation(ValidateUsing.class);
        Class ruleDataType = Reflector.getRuleDataType(validateUsing);
        SARIPAAR_REGISTRY.register(viewType, ruleDataType, viewDataAdapter, annotation);
    }

    /**
     * Registers a {@link ViewDataAdapter} for the given
     * {@link Component}.
     *
     * @param viewType        The {@link Component} for which a
     *                        {@link ViewDataAdapter} is being registered.
     * @param viewDataAdapter A {@link ViewDataAdapter} instance.
     * @param <VIEW>          The {@link Component} type.
     * @param <DATA_TYPE>     The {@link ViewDataAdapter} type.
     */
    public <VIEW extends Component, DATA_TYPE> void registerAdapter(
            final Class<VIEW> viewType, final ViewDataAdapter<VIEW, DATA_TYPE> viewDataAdapter) {
        assertNotNull(viewType, "viewType");
        assertNotNull(viewDataAdapter, "viewDataAdapter");

        HashMap<Class<?>, ViewDataAdapter> dataTypeAdapterMap = mRegisteredAdaptersMap.get(viewType);
        if (dataTypeAdapterMap == null) {
            dataTypeAdapterMap = new HashMap<Class<?>, ViewDataAdapter>();
            mRegisteredAdaptersMap.put(viewType, dataTypeAdapterMap);
        }

        // Find adapter's data type
        Method getDataMethod = Reflector.findGetDataMethod(viewDataAdapter.getClass());
        Class<?> adapterDataType = getDataMethod.getReturnType();

        dataTypeAdapterMap.put(adapterDataType, viewDataAdapter);
    }

    /**
     * Set a {@link ValidationListener} to the
     * {@link Validator}.
     *
     * @param validationListener A {@link ValidationListener}
     *                           instance. null throws an {@link IllegalArgumentException}.
     */
    public void setValidationListener(final ValidationListener validationListener) {
        assertNotNull(validationListener, "validationListener");
        this.mValidationListener = validationListener;
    }

    /**
     * Set a {@link ViewValidatedAction} to the
     * {@link Validator}.
     *
     * @param viewValidatedAction A {@link ViewValidatedAction}
     *                            instance.
     */
    public void setViewValidatedAction(final ViewValidatedAction viewValidatedAction) {
        this.mViewValidatedAction = viewValidatedAction;
    }

    /**
     * Set the validation {@link Mode} for the current
     * {@link Validator} instance.
     *
     * @param validationMode {@link Mode#BURST} or {@link Mode#IMMEDIATE}, null throws an
     *                       {@link IllegalArgumentException}.
     */
    public void setValidationMode(final Mode validationMode) {
        assertNotNull(validationMode, "validationMode");
        this.mValidationMode = validationMode;
    }

    /**
     * Gets the current {@link Mode}.
     *
     * @return The current validation mode of the {@link Validator}.
     */
    public Mode getValidationMode() {
        return mValidationMode;
    }

    /**
     * Configures the validator to validate invisible views.
     *
     * @param validate {@code true} includes invisible views during validation.
     */
    public void validateInvisibleViews(final boolean validate) {
        this.mValidateInvisibleViews = validate;
    }

    /**
     * Validates all {@link Component}s with {@link Rule}s.
     * When validating in {@link Mode#IMMEDIATE}, all
     * {@link Component} fields must be ordered using the
     * {@link Order} annotation.
     */
    public void validate() {
        validate(false);
    }

    /**
     * Validates all {@link Component}s before the specified {@link Component}
     * parameter. {@link Component} fields MUST be ordered using the
     * {@link Order} annotation.
     *
     * @param view A {@link Component}.
     */
    public void validateBefore(final Component view) {
        validateBefore(view, false);
    }

    /**
     * Validates all {@link Component}s till the specified {@link Component}
     * parameter. {@link Component} fields MUST be ordered using the
     * {@link Order} annotation.
     *
     * @param view A {@link Component}.
     */
    public void validateTill(final Component view) {
        validateTill(view, false);
    }

    /**
     * Validates all {@link Component}s with {@link Rule}s.
     * When validating in {@link Mode#IMMEDIATE}, all
     * {@link Component} fields must be ordered using the
     * {@link Order} annotation. Asynchronous calls will cancel
     * any pending or ongoing asynchronous validation and start a new one.
     *
     * @param async true if asynchronous, false otherwise.
     */
    public void validate(final boolean async) {
        createRulesSafelyAndLazily(false);

        Component lastView = getLastView();
        if (Mode.BURST.equals(mValidationMode)) {
            validateUnorderedFieldsWithCallbackTill(lastView, async);
        } else if (Mode.IMMEDIATE.equals(mValidationMode)) {
            String reasonSuffix = String.format("in %s mode.", Mode.IMMEDIATE.toString());
            validateOrderedFieldsWithCallbackTill(lastView, reasonSuffix, async);
        } else {
            throw new RuntimeException("This should never happen!");
        }
    }

    /**
     * Validates all {@link Component}s before the specified {@link Component}
     * parameter. {@link Component} fields MUST be ordered using the
     * {@link Order} annotation. Asynchronous calls will cancel
     * any pending or ongoing asynchronous validation and start a new one.
     *
     * @param view  A {@link Component}.
     * @param async true if asynchronous, false otherwise.
     */
    public void validateBefore(final Component view, final boolean async) {
        createRulesSafelyAndLazily(false);
        Component previousView = getViewBefore(view);
        validateOrderedFieldsWithCallbackTill(previousView, "when using 'validateBefore(View)'.",
                async);
    }

    /**
     * Validates all {@link Component}s till the specified {@link Component}
     * parameter. {@link Component} fields MUST be ordered using the
     * {@link Order} annotation. Asynchronous calls will cancel
     * any pending or ongoing asynchronous validation and start a new one.
     *
     * @param view  A {@link Component}.
     * @param async true if asynchronous, false otherwise.
     */
    public void validateTill(final Component view, final boolean async) {
        validateOrderedFieldsWithCallbackTill(view, "when using 'validateTill(View)'.", async);
    }

    /**
     * Used to find if an asynchronous validation task is running. Useful only when you run the
     * {@link Validator} in asynchronous mode.
     *
     * @return true if the asynchronous task is running, false otherwise.
     */
    public boolean isValidating() {
        return mAsyncValidationTask != null
                && mAsyncValidationTask.getStatus() != AsyncTask.Status.FINISHED;
    }

    /**
     * Cancels a running asynchronous validation task.
     *
     * @return true if a running asynchronous task was cancelled, false otherwise.
     */
    public boolean cancelAsync() {
        boolean cancelled = false;
        if (mAsyncValidationTask != null) {
            cancelled = mAsyncValidationTask.cancel(true);
            mAsyncValidationTask = null;
        }

        return cancelled;
    }

    /**
     * Add one or more {@link QuickRule}s for a {@link Component}.
     *
     * @param view       A {@link Component} for which
     *                   {@link QuickRule}(s) are to be added.
     * @param quickRules Varargs of {@link QuickRule}s.
     * @param <VIEW>     The {@link Component} type for which the
     *                   {@link QuickRule}s are being registered.
     */
    public <VIEW extends Component> void put(final VIEW view, final QuickRule<VIEW>... quickRules) {
        assertNotNull(view, "view");
        assertNotNull(quickRules, "quickRules");
        if (quickRules.length == 0) {
            throw new IllegalArgumentException("'quickRules' cannot be empty.");
        }

        if (mValidationContext == null) {
            mValidationContext = new ValidationContext(view.getContext());
        }

        // Create rules
        createRulesSafelyAndLazily(true);

        // If all fields are ordered, then this field should be ordered too
        if (mOrderedFields && !mViewRulesMap.containsKey(view)) {
            String message = String.format("All fields are ordered, so this `%s` should be "
                    + "ordered too, declare the view as a field and add the `@Order` "
                    + "annotation.", view.getClass().getName());
            throw new IllegalStateException(message);
        }

        // If there are no rules, create an empty list
        ArrayList<Pair<Rule, ViewDataAdapter>> ruleAdapterPairs = mViewRulesMap.get(view);
        ruleAdapterPairs = ruleAdapterPairs == null
                ? new ArrayList<Pair<Rule, ViewDataAdapter>>() : ruleAdapterPairs;

        // Add the quick rule to existing rules
        for (int i = 0, n = quickRules.length; i < n; i++) {
            QuickRule quickRule = quickRules[i];
            if (quickRule != null) {
                ruleAdapterPairs.add(new Pair(quickRule, null));
            }
        }
        Collections.sort(ruleAdapterPairs, mSequenceComparator);
        mViewRulesMap.put(view, ruleAdapterPairs);
    }

    /**
     * Remove all {@link Rule}s for the given {@link Component}.
     *
     * @param view The {@link Component} whose rules should be removed.
     */
    public void removeRules(final Component view) {
        assertNotNull(view, "view");
        if (mViewRulesMap == null) {
            createRulesSafelyAndLazily(false);
        }
        mViewRulesMap.remove(view);
    }

    static boolean isSaripaarAnnotation(final Class<? extends Annotation> annotation) {
        return SARIPAAR_REGISTRY.getRegisteredAnnotations().contains(annotation);
    }

    /* ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     *  Private Methods
     * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
     */
    private static void assertNotNull(final Object object, final String argumentName) {
        if (object == null) {
            String message = String.format("'%s' cannot be null.", argumentName);
            throw new IllegalArgumentException(message);
        }
    }

    private void createRulesSafelyAndLazily(final boolean addingQuickRules) {
        // Create rules lazily, because we don't have to worry about the order of
        // instantiating the Validator.
        if (mViewRulesMap == null) {
            final List<Field> annotatedFields = getSaripaarAnnotatedFields(mController.getClass());
            mViewRulesMap = createRules(annotatedFields);
            mValidationContext.setViewRulesMap(mViewRulesMap);
        }

        if (!addingQuickRules && mViewRulesMap.size() == 0) {
            String message = "No rules found. You must have at least one rule to validate. "
                    + "If you are using custom annotations, make sure that you have registered "
                    + "them using the 'Validator.register()' method.";
            throw new IllegalStateException(message);
        }
    }

    private List<Field> getSaripaarAnnotatedFields(final Class<?> controllerClass) {
        Set<Class<? extends Annotation>> saripaarAnnotations =
                SARIPAAR_REGISTRY.getRegisteredAnnotations();

        List<Field> annotatedFields = new ArrayList<Field>();
        List<Field> controllerViewFields = getControllerViewFields(controllerClass);
        for (int i = 0, n = controllerViewFields.size(); i < n; i++) {
            Field field = controllerViewFields.get(i);
            if (isSaripaarAnnotatedField(field, saripaarAnnotations)) {
                annotatedFields.add(field);
            }
        }

        // Sort
        SaripaarFieldsComparator comparator = new SaripaarFieldsComparator();
        Collections.sort(annotatedFields, comparator);
        mOrderedFields = annotatedFields.size() == 1
                ? annotatedFields.get(0).getAnnotation(Order.class) != null
                : annotatedFields.size() != 0 && comparator.areOrderedFields();

        return annotatedFields;
    }

    private List<Field> getControllerViewFields(final Class<?> controllerClass) {
        List<Field> controllerViewFields = new ArrayList<Field>();

        // Fields declared in the controller
        controllerViewFields.addAll(getViewFields(controllerClass));

        // Inherited fields
        Class<?> superClass = controllerClass.getSuperclass();
        while (!superClass.equals(Object.class)) {
            List<Field> viewFields = getViewFields(superClass);
            if (viewFields.size() > 0) {
                controllerViewFields.addAll(viewFields);
            }
            superClass = superClass.getSuperclass();
        }

        return controllerViewFields;
    }

    private List<Field> getViewFields(final Class<?> clazz) {
        List<Field> viewFields = new ArrayList<Field>();
        Field[] declaredFields = clazz.getDeclaredFields();
        for (int i = 0, n = declaredFields.length; i < n; i++) {
            Field field = declaredFields[i];
            if (Component.class.isAssignableFrom(field.getType())) {
                viewFields.add(field);
            }
        }

        return viewFields;
    }

    private boolean isSaripaarAnnotatedField(final Field field,
                                             final Set<Class<? extends Annotation>> registeredAnnotations) {
        boolean hasOrderAnnotation = field.getAnnotation(Order.class) != null;
        boolean hasSaripaarAnnotation = false;

        if (!hasOrderAnnotation) {
            Annotation[] annotations = field.getAnnotations();
            for (int i = 0, n = annotations.length; i < n; i++) {
                Annotation annotation = annotations[i];
                hasSaripaarAnnotation = registeredAnnotations.contains(annotation.annotationType());
                if (hasSaripaarAnnotation) {
                    break;
                }
            }
        }

        return hasOrderAnnotation || hasSaripaarAnnotation;
    }

    private Map<Component, ArrayList<Pair<Rule, ViewDataAdapter>>> createRules(
            final List<Field> annotatedFields) {

        final Map<Component, ArrayList<Pair<Rule, ViewDataAdapter>>> viewRulesMap =
                new LinkedHashMap<Component, ArrayList<Pair<Rule, ViewDataAdapter>>>();

        Component view;
        for (int i = 0, n = annotatedFields.size(); i < n; i++) {
            Field field = annotatedFields.get(i);
            final ArrayList<Pair<Rule, ViewDataAdapter>> ruleAdapterPairs =
                    new ArrayList<Pair<Rule, ViewDataAdapter>>();
            final Annotation[] fieldAnnotations = field.getAnnotations();

            // @Optional
            final boolean hasOptionalAnnotation = hasOptionalAnnotation(fieldAnnotations);
            if (hasOptionalAnnotation && mOptionalViewsMap == null) {
                mOptionalViewsMap = new HashMap<Component,
                        ArrayList<Pair<Annotation, ViewDataAdapter>>>();
            }

            view = getView(field);
            for (int j = 0, nAnnotations = fieldAnnotations.length; j < nAnnotations; j++) {
                Annotation annotation = fieldAnnotations[j];
                if (isSaripaarAnnotation(annotation.annotationType())) {
                    Pair<Rule, ViewDataAdapter> ruleAdapterPair =
                            getRuleAdapterPair(annotation, field);
                    ruleAdapterPairs.add(ruleAdapterPair);

                    // @Optional
                    if (hasOptionalAnnotation) {
                        ArrayList<Pair<Annotation, ViewDataAdapter>> pairs =
                                mOptionalViewsMap.get(view);
                        if (pairs == null) {
                            pairs = new ArrayList<Pair<Annotation, ViewDataAdapter>>();
                        }
                        pairs.add(new Pair(annotation, ruleAdapterPair.s));
                        mOptionalViewsMap.put(view, pairs);
                    }
                }
            }

            Collections.sort(ruleAdapterPairs, mSequenceComparator);
            viewRulesMap.put(view, ruleAdapterPairs);
        }

        return viewRulesMap;
    }

    private boolean hasOptionalAnnotation(final Annotation[] annotations) {
        if (annotations != null && annotations.length > 0) {
            for (int i = 0, n = annotations.length; i < n; i++) {
                if (Optional.class.equals(annotations[i].annotationType())) {
                    return true;
                }
            }
        }
        return false;
    }

    private Pair<Rule, ViewDataAdapter> getRuleAdapterPair(final Annotation saripaarAnnotation,
                                                           final Field viewField) {
        final Class<? extends Annotation> annotationType = saripaarAnnotation.annotationType();
        final Class<?> viewFieldType = viewField.getType();
        final Class<?> ruleDataType = Reflector.getRuleDataType(saripaarAnnotation);

        final ViewDataAdapter dataAdapter = getDataAdapter(annotationType, viewFieldType,
                ruleDataType);

        // If no matching adapter is found, throw.
        if (dataAdapter == null) {
            String viewType = viewFieldType.getName();
            String message = String.format(
                    "To use '%s' on '%s', register a '%s' that returns a '%s' from the '%s'.",
                    annotationType.getName(),
                    viewType,
                    ViewDataAdapter.class.getName(),
                    ruleDataType.getName(),
                    viewType);
            throw new UnsupportedOperationException(message);
        }

        if (mValidationContext == null) {
            mValidationContext = new ValidationContext(getContext(viewField));
        }

        final Class<? extends AnnotationRule> ruleType = getRuleType(saripaarAnnotation);
        final AnnotationRule rule = Reflector.instantiateRule(ruleType,
                saripaarAnnotation, mValidationContext);

        return new Pair<Rule, ViewDataAdapter>(rule, dataAdapter);
    }

    private ViewDataAdapter getDataAdapter(final Class<? extends Annotation> annotationType,
                                           final Class<?> viewFieldType, final Class<?> adapterDataType) {

        // Get an adapter from the stock registry
        ViewDataAdapter dataAdapter = SARIPAAR_REGISTRY.getDataAdapter(
                annotationType, (Class) viewFieldType);

        // If we are unable to find a Saripaar stock adapter, check the registered adapters
        if (dataAdapter == null) {
            HashMap<Class<?>, ViewDataAdapter> dataTypeAdapterMap =
                    mRegisteredAdaptersMap.get(viewFieldType);
            dataAdapter = dataTypeAdapterMap != null
                    ? dataTypeAdapterMap.get(adapterDataType)
                    : null;
        }

        return dataAdapter;
    }

    private Context getContext(final Field viewField) {
        Context context = null;
        try {
            if (!viewField.isAccessible()) {
                viewField.setAccessible(true);
            }
            Component view = (Component) viewField.get(mController);
            context = view.getContext();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return context;
    }

    private Class<? extends AnnotationRule> getRuleType(final Annotation ruleAnnotation) {
        ValidateUsing validateUsing = ruleAnnotation.annotationType()
                .getAnnotation(ValidateUsing.class);
        return validateUsing != null ? validateUsing.value() : null;
    }

    private Component getView(final Field field) {
        Component view = null;
        try {
            field.setAccessible(true);
            view = (Component) field.get(mController);

            if (view == null) {
                String message = String.format("'%s %s' is null.",
                        field.getType().getSimpleName(), field.getName());
                throw new IllegalStateException(message);
            }
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        return view;
    }

    private void validateUnorderedFieldsWithCallbackTill(final Component view, final boolean async) {
        validateFieldsWithCallbackTill(view, false, null, async);
    }

    private void validateOrderedFieldsWithCallbackTill(final Component view, final String reasonSuffix,
                                                       final boolean async) {
        validateFieldsWithCallbackTill(view, true, reasonSuffix, async);
    }

    private void validateFieldsWithCallbackTill(final Component view, final boolean orderedFields,
                                                final String reasonSuffix, final boolean async) {
        createRulesSafelyAndLazily(false);
        if (async) {
            if (mAsyncValidationTask != null) {
                mAsyncValidationTask.cancel(true);
            }
            mAsyncValidationTask = new AsyncValidationTask(view, orderedFields, reasonSuffix);
            mAsyncValidationTask.execute((Void[]) null);
        } else {
            triggerValidationListenerCallback(validateTill(view, orderedFields, reasonSuffix));
        }
    }

    private synchronized ValidationReport validateTill(final Component view,
                                                       final boolean requiresOrderedRules, final String reasonSuffix) {
        // Do we need ordered rules?
        if (requiresOrderedRules) {
            assertOrderedFields(mOrderedFields, reasonSuffix);
        }

        // Have we registered a validation listener?
        assertNotNull(mValidationListener, "validationListener");

        // Everything good. Bingo! validate ;)
        return getValidationReport(view, mViewRulesMap, mValidationMode);
    }

    private void triggerValidationListenerCallback(final ValidationReport validationReport) {
        final List<ValidationError> validationErrors = validationReport.errors;

        if (validationErrors.size() == 0 && !validationReport.hasMoreErrors) {
            mValidationListener.onValidationSucceeded();
        } else {
            mValidationListener.onValidationFailed(validationErrors);
        }
    }

    private void assertOrderedFields(final boolean orderedRules, final String reasonSuffix) {
        if (!orderedRules) {
            String message = "Rules are unordered, all view fields should be ordered "
                    + "using the '@Order' annotation " + reasonSuffix;
            throw new IllegalStateException(message);
        }
    }

    private ValidationReport getValidationReport(final Component targetView,
                                                 final Map<Component, ArrayList<Pair<Rule, ViewDataAdapter>>> viewRulesMap,
                                                 final Mode validationMode) {

        final List<ValidationError> validationErrors = new ArrayList<ValidationError>();

        // Don't add errors for views that are placed after the specified view in validateTill()
        boolean addErrorToReport = targetView != null;

        // Does the form have more errors? Used in validateTill()
        boolean hasMoreErrors = false;

        for (Map.Entry<Component, ArrayList<Pair<Rule, ViewDataAdapter>>> entry : viewRulesMap.entrySet()) {
            Component view = entry.getKey();
            List<Pair<Rule, ViewDataAdapter>> ruleAdapterPairs = entry.getValue();

            // @Optional
            boolean isOptional = mOptionalViewsMap != null && mOptionalViewsMap.containsKey(view);
            if (isOptional && containsOptionalValue(view)) {
                continue;
            }

            // Validate all the rules for the given view.
            List<Rule> failedRules = null;
            for (int i = 0, nRules = ruleAdapterPairs.size(); i < nRules; i++) {

                // Skip views that are invisible and disabled
                boolean disabledView = !view.isEnabled();
                boolean skipView = !view.isComponentDisplayed() && !mValidateInvisibleViews;
                if (disabledView || skipView) {
                    continue;
                }

                Pair<Rule, ViewDataAdapter> ruleAdapterPair = ruleAdapterPairs.get(i);
                Rule failedRule = validateViewWithRule(
                        view, ruleAdapterPair.f, ruleAdapterPair.s);
                boolean isLastRuleForView = i + 1 == nRules;

                if (failedRule != null) {
                    if (addErrorToReport) {
                        if (failedRules == null) {
                            failedRules = new ArrayList<Rule>();
                            validationErrors.add(new ValidationError(view, failedRules));
                        }
                        failedRules.add(failedRule);
                    } else {
                        hasMoreErrors = true;
                    }

                    if (Mode.IMMEDIATE.equals(validationMode) && isLastRuleForView) {
                        break;
                    }
                }

                // Don't add reports for subsequent views
                if (view.equals(targetView) && isLastRuleForView) {
                    addErrorToReport = false;
                }
            }

            // Callback if a view passes all rules
            boolean viewPassedAllRules = (failedRules == null || failedRules.size() == 0)
                    && !hasMoreErrors;
            if (viewPassedAllRules && mViewValidatedAction != null) {
                triggerViewValidatedCallback(mViewValidatedAction, view);
            }
        }

        return new ValidationReport(validationErrors, hasMoreErrors);
    }

    private boolean containsOptionalValue(final Component view) {
        ArrayList<Pair<Annotation, ViewDataAdapter>> annotationAdapterPairs
                = mOptionalViewsMap.get(view);

        for (int i = 0, n = annotationAdapterPairs.size(); i < n; i++) {
            Pair<Annotation, ViewDataAdapter> pair = annotationAdapterPairs.get(i);
            ViewDataAdapter adapter = pair.s;
            Annotation ruleAnnotation = pair.f;

            if (adapter.containsOptionalValue(view, ruleAnnotation)) {
                return true;
            }
        }

        return false;
    }

    private Rule validateViewWithRule(final Component view, final Rule rule,
                                      final ViewDataAdapter dataAdapter) {

        boolean valid = false;
        if (rule instanceof AnnotationRule) {
            Object data;

            try {
                data = dataAdapter.getData(view);
                valid = rule.isValid(data);
            } catch (ConversionException e) {
                valid = false;
                e.printStackTrace();
            }
        } else if (rule instanceof QuickRule) {
            valid = rule.isValid(view);
        }

        return valid ? null : rule;
    }

    private void triggerViewValidatedCallback(final ViewValidatedAction viewValidatedAction,
                                              final Component view) {
        boolean isOnMainThread = EventRunner.current() == EventRunner.getMainEventRunner();
        if (isOnMainThread) {
            viewValidatedAction.onAllRulesPassed(view);
        } else {
            runOnMainThread(new Runnable() {
                @Override
                public void run() {
                    viewValidatedAction.onAllRulesPassed(view);
                }
            });
        }
    }

    private void runOnMainThread(final Runnable runnable) {
        if (mViewValidatedActionHandler == null) {
            mViewValidatedActionHandler = new EventHandler(EventRunner.getMainEventRunner());
        }
        mViewValidatedActionHandler.postTask(runnable);
    }

    private Component getLastView() {
        final Set<Component> views = mViewRulesMap.keySet();

        Component lastView = null;
        for (Component view : views) {
            lastView = view;
        }

        return lastView;
    }

    private Component getViewBefore(final Component view) {
        ArrayList<Component> views = new ArrayList<Component>(mViewRulesMap.keySet());

        Component currentView;
        Component previousView = null;
        for (int i = 0, n = views.size(); i < n; i++) {
            currentView = views.get(i);
            if (currentView == view) {
                previousView = i > 0 ? views.get(i - 1) : null;
                break;
            }
        }

        return previousView;
    }

    /**
     * Listener with callback methods that notifies the outcome of validation.
     *
     * @author Ragunath Jawahar {@literal <rj@mobsandgeeks.com>}
     * @since 1.0
     */
    public interface ValidationListener {

        /**
         * Called when all {@link Rule}s pass.
         */
        void onValidationSucceeded();

        /**
         * Called when one or several {@link Rule}s fail.
         *
         * @param errors List containing references to the {@link Component}s and
         *               {@link Rule}s that failed.
         */
        void onValidationFailed(List<ValidationError> errors);
    }

    /**
     * Interface that provides a callback when all {@link Rule}s
     * associated with a {@link Component} passes.
     *
     * @author Ragunath Jawahar {@literal <rj@mobsandgeeks.com>}
     * @since 2.0
     */
    public interface ViewValidatedAction {

        /**
         * Called when all rules associated with the {@link Component} passes.
         *
         * @param view The {@link Component} that has passed validation.
         */
        void onAllRulesPassed(Component view);
    }

    /**
     * Validation mode.
     *
     * @author Ragunath Jawahar {@literal <rj@mobsandgeeks.com>}
     * @since 2.0
     */
    public enum Mode {

        /**
         * BURST mode will validate all rules in all views before calling the
         * {@link ValidationListener#onValidationFailed(List)}
         * callback. Ordering and sequencing is optional.
         */
        BURST,

        /**
         * IMMEDIATE mode will stop the validation after validating all the rules
         * of the first failing view. Requires ordered rules, sequencing is optional.
         */
        IMMEDIATE
    }

    static class ValidationReport {
        List<ValidationError> errors;
        boolean hasMoreErrors;

        ValidationReport(final List<ValidationError> errors, final boolean hasMoreErrors) {
            this.errors = errors;
            this.hasMoreErrors = hasMoreErrors;
        }
    }

    private class AsyncValidationTask extends AsyncTask<Void, Void, ValidationReport> {
        private final Component mView;
        private final boolean mOrderedRules;
        private final String mReasonSuffix;

        AsyncValidationTask(final Component view, final boolean orderedRules,
                            final String reasonSuffix) {
            this.mView = view;
            this.mOrderedRules = orderedRules;
            this.mReasonSuffix = reasonSuffix;
        }

        @Override
        protected ValidationReport doInBackground(final Void... params) {
            return validateTill(mView, mOrderedRules, mReasonSuffix);
        }

        @Override
        protected void onPostExecute(final ValidationReport validationReport) {
            triggerValidationListenerCallback(validationReport);
        }
    }

    static {
        // CheckBoxBooleanAdapter
        SARIPAAR_REGISTRY.register(Checkbox.class, Boolean.class,
                new CheckBoxBooleanAdapter(),
                AssertFalse.class, AssertTrue.class, Checked.class);

        // RadioGroupBooleanAdapter
        SARIPAAR_REGISTRY.register(RadioContainer.class, Boolean.class,
                new RadioGroupBooleanAdapter(),
                Checked.class);

        // RadioButtonBooleanAdapter
        SARIPAAR_REGISTRY.register(RadioButton.class, Boolean.class,
                new RadioButtonBooleanAdapter(),
                AssertFalse.class, AssertTrue.class, Checked.class);

        // SpinnerIndexAdapter
        SARIPAAR_REGISTRY.register(Picker.class, Integer.class,
                new SpinnerIndexAdapter(),
                Select.class);

        // TextViewDoubleAdapter
        SARIPAAR_REGISTRY.register(DecimalMax.class, DecimalMin.class);

        // TextViewIntegerAdapter
        SARIPAAR_REGISTRY.register(Max.class, Min.class);

        // TextViewStringAdapter
        SARIPAAR_REGISTRY.register(
                ConfirmEmail.class, ConfirmPassword.class, CreditCard.class,
                Digits.class, Domain.class, Email.class, Future.class,
                IpAddress.class, Isbn.class, Length.class, NotEmpty.class,
                Password.class, Past.class, Pattern.class, Url.class);
    }
}
