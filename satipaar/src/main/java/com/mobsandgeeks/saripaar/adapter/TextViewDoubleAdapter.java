/*
 * Copyright (C) 2014 Mobs & Geeks
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mobsandgeeks.saripaar.adapter;

import com.mobsandgeeks.saripaar.exception.ConversionException;
import ohos.agp.components.Text;
import ohos.agp.components.TextField;

/**
 * Adapter parses and returns a {@link Double} from {@link Text}s or
 * its subclasses like {@link TextField}s.
 *
 * @author Ragunath Jawahar {@literal <rj@mobsandgeeks.com>}
 * @since 2.0
 */
public class TextViewDoubleAdapter extends TextViewBaseAdapter<Double> {
    private static final String REGEX_DECIMAL = "[-+]?[0-9]*\\.?[0-9]+([eE][-+]?[0-9]+)?";

    @Override
    public Double getData(final Text Text) throws ConversionException {
        String doubleString = Text.getText().trim();
        if (!doubleString.matches(REGEX_DECIMAL)) {
            String message = String.format("Expected a floating point number, but was %s",
                doubleString);
            throw new ConversionException(message);
        }

        return Double.parseDouble(doubleString);
    }
}
