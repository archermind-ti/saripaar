/*
 * Copyright (C) 2014 Mobs & Geeks
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mobsandgeeks.saripaar.adapter;


import ohos.agp.components.Checkbox;

import java.lang.annotation.Annotation;

/**
 * Adapter returns a {@link Boolean} value from a {@link Checkbox}.
 *
 * @author Ragunath Jawahar {@literal <rj@mobsandgeeks.com>}
 * @since 2.0
 */
public class CheckBoxBooleanAdapter implements ViewDataAdapter<Checkbox, Boolean> {

    @Override
    public Boolean getData(final Checkbox Checkbox) {
        return Checkbox.isChecked();
    }

    @Override
    public <T extends Annotation> boolean containsOptionalValue(final Checkbox Checkbox,
            final T ruleAnnotation) {
        return !Checkbox.isChecked();
    }
}
