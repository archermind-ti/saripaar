# saripaar

### 简介

Saripaar 是一个简单、功能丰富且功能强大的基于规则的 OHOS UI 表单验证库。 它是适用于 OHOS的最简单的 UI 验证库。

### 功能

1. 使用注解的声明式验证

2. 可扩展，允许自定义注解

3. 同步和异步验证，无需担心线程问题

4. 支持BURST和IMMEDIATE模式

5. 使用规则隔离验证逻辑

6. 兼容其它注解的库和框架，例如：ButterKnife

7. 目前定义的注解包括：

   ```java
   @AssertFalse
   @AssertTrue
   @Checked
   @ConfirmEmail
   @ConfirmPassword
   @CreditCard
   @DecimalMax
   @DecimalMin
   @Digits
   @Domain
   @Email
   @Future
   @IpAddress
   @Isbn
   @Length
   @MainThread
   @Max
   @Min
   @NotEmpty
   @Optional
   @Or
   @Order
   @Password
   @Past
   @Pattern
   @Select
   @StringRes
   @Url
   @ValidateUsing
   @WorkerThread
   ```

### 效果展示

![sample.gif](sample.gif) 

### 集成

1. 首先在project的build.gradle中添加mavenCentral()仓库

   ```groovy
   allprojects {
       repositories {
           mavenCentral()
       }
   }
   ```

   

2. 在需要使用的module的build.gradle中添加依赖：

   ```groovy
   dependencies {
       implementation 'com.gitee.archermind-ti:saripaar:1.0.1'
   }
   ```

   

### 使用说明

1. 使用Saripaar注解来标注您的控件

   ``` java
   @NotEmpty
   @Email
   private TextField emailEditText;
   
   @Password(min = 6, scheme = Password.Scheme.ALPHA_NUMERIC_MIXED_CASE_SYMBOLS)
   private TextField passwordEditText;
   
   @ConfirmPassword
   private TextField confirmPasswordEditText;
   
   @Checked(message = "You must agree to the terms.")
   private Checkbox iAgreeCheckBox;
   ```

   注解是安全的。 仅在使用`Validator.validateTill(Component)`和`Validator.validateBefore(Component)`或在`IMMEDIATE`下执行有序验证时，才需要`@Order`注解。

2. 实例化一个`Validator`

   ```java
       @Override
       protected void onStart(Intent intent) {
           super.onStart(intent);
   		//code...
   
           // Validator
           validator = new Validator(this);
           validator.setValidationListener(this);
   
          	//code...
       }
   ```

   您将需要一个`Validator`和`ValidationListener`来接收有关验证事件的回调。

3. 实现`ValidationListener`

   ```java
   public class ConfirmPasswordNoPasswordSlice extends AbilitySlice
           implements Validator.ValidationListener {
   
      	//Code...
   
       @Override
       public void onValidationSucceeded() {
           //Validator success
       }
   
       @Override
       public void onValidationFailed(List<ValidationError> errors) {
           for (ValidationError error : errors) {
               Component view = error.getView();
               String message = error.getCollatedErrorMessage(this);
   
               // Display error messages
           }
       }
   }
   ```

   * `onValidationSucceeded()` - 当所有`Component`均通过所有验证时调用。

   * `onValidationFailed(List<ValidationError> errors)` - 发生验证错误时调用。

4. 验证

   ``` java
   mSaripaarButton.setClickedListener(new Component.ClickedListener() {
               @Override
               public void onClick(Component component) {
                   mValidator.validate();
               }
           });
   ```

   调用`Validator.validate()`运行验证，并通过`ValidationListener`上的回调返回结果。你可以通过调用`Validator.validate(true)`方法在异步任务上运行验证。

### 编译说明

1. 将项目通过git clone 至本地
2. 使用DevEco Studio 打开该项目，然后等待Gradle 构建完成
3. 点击`Run`运行即可（真机运行可能需要配置签名）

### License

```
Copyright 2012 - 2015 Mobs & Geeks

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```
