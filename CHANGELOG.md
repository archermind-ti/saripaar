# Releases

## v1.0.1

1. 替换敏感名称

## v1.0.0

移植完成，支持功能如下：

1. 使用注解的声明式验证

2. 可扩展，允许自定义注解

3. 同步和异步验证，无需担心线程问题

4. 支持BURST和IMMEDIATE模式

5. 使用规则隔离验证逻辑

6. 兼容其它注解的库和框架，例如：ButterKnife

7. 目前定义的注解包括：

   ```java
   @AssertFalse
   @AssertTrue
   @Checked
   @ConfirmEmail
   @ConfirmPassword
   @CreditCard
   @DecimalMax
   @DecimalMin
   @Digits
   @Domain
   @Email
   @Future
   @IpAddress
   @Isbn
   @Length
   @MainThread
   @Max
   @Min
   @NotEmpty
   @Optional
   @Or
   @Order
   @Password
   @Past
   @Pattern
   @Select
   @StringRes
   @Url
   @ValidateUsing
   @WorkerThread
   ```

