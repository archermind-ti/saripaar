/*
 * Copyright (C) 2015 Mobs & Geeks
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mobsandgeeks.saripaar.slice;


import com.mobsandgeeks.saripaar.ResourceTable;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.ConfirmPassword;
import com.mobsandgeeks.saripaar.annotation.Password;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.components.TextField;

import java.util.List;


public class ConfirmPasswordWithPasswordSlice extends Ability
        implements Component.ClickedListener, Validator.ValidationListener {

    @Password
    private TextField mPasswordEditText;

    @ConfirmPassword
    private TextField mConfirmPasswordEditText;

    private Text mResultTextView;
    private Button mSaripaarButton;

    private Validator mValidator;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_slice_confirm_password_with_password);

        mPasswordEditText = (TextField) findComponentById(ResourceTable.Id_passwordEditText);
        mConfirmPasswordEditText = (TextField) findComponentById(ResourceTable.Id_confirmPasswordEditText);
        mResultTextView = (Text) findComponentById(ResourceTable.Id_resultTextView);
        mSaripaarButton = (Button) findComponentById(ResourceTable.Id_saripaarButton);

        // Event listeners
        mSaripaarButton.setClickedListener(this);

        // Validation
        mValidator = new Validator(this);
        mValidator.setValidationListener(this);
    }

    @Override
    public void onClick(Component component) {
        mValidator.validate();
    }

    @Override
    public void onValidationSucceeded() {
        mResultTextView.setText("success");
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        mResultTextView.setText(Common.getFailedFieldNames(errors));
    }
}
