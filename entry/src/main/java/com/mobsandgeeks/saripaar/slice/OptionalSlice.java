package com.mobsandgeeks.saripaar.slice;


import com.mobsandgeeks.saripaar.ResourceTable;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Checked;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.Optional;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;

import java.util.List;

/**
 * @author Ragunath Jawahar {@literal <rj@mobsandgeeks.com>}
 */
public class OptionalSlice extends Ability
        implements Validator.ValidationListener, Component.ClickedListener {

    // Fields
    @Optional
    @Email
    TextField mEmailEditText;
    @Optional
    @Checked
    Checkbox mEmailUpdatesEditText;

    // Attributes
    private Validator mValidator;
    private Text
            mResultTextView;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_slice_optional);

        // UI References
        mEmailEditText = (TextField) findComponentById(ResourceTable.Id_emailEditText);
        mEmailUpdatesEditText = (Checkbox) findComponentById(ResourceTable.Id_emailUpdatesCheckBox);
        mResultTextView = (Text) findComponentById(ResourceTable.Id_resultTextView);
        Button saripaarButton = (Button) findComponentById(ResourceTable.Id_saripaarButton);

        // Validator
        mValidator = new Validator(this);
        mValidator.setValidationListener(this);

        // Event listeners
        saripaarButton.setClickedListener(this);
    }

    @Override
    public void onClick(Component component) {
        mValidator.validate();
    }

    @Override
    public void onValidationSucceeded() {
        mResultTextView.setText("success");
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        mResultTextView.setText(Common.getFailedFieldNames(errors));
    }
}
