package com.mobsandgeeks.saripaar.slice;

import com.mobsandgeeks.saripaar.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;

public class TestAbilitySlice extends AbilitySlice implements Component.ClickedListener {

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_test);
        ComponentContainer group = (ComponentContainer) findComponentById(ResourceTable.Id_content);
        for (int i = 0; i < group.getChildCount(); i++) {
            group.getComponentAt(i).setClickedListener(this);
        }
    }

    @Override
    public void onClick(Component component) {
        Class<?> slice;
        switch (component.getId()) {
            case ResourceTable.Id_btn_cpnp:
                slice = ConfirmPasswordNoPasswordSlice.class;
                break;
            case ResourceTable.Id_btn_cpsp:
                slice = ConfirmPasswordSeveralPasswordsSlice.class;
                break;
            case ResourceTable.Id_btn_cpwp:
                slice = ConfirmPasswordWithPasswordSlice.class;
                break;
            case ResourceTable.Id_btn_ca:
                slice = CustomAnnotationSlice.class;
                break;
            case ResourceTable.Id_btn_cawa:
                slice = CustomAnnotationWithAdapterSlice.class;
                break;
            case ResourceTable.Id_btn_optional:
                slice = OptionalSlice.class;
                break;
            case ResourceTable.Id_btn_os:
                slice = OrderedSequencingSlice.class;
                break;
            case ResourceTable.Id_btn_ovb:
                slice = OrderedValidateBeforeSlice.class;
                break;
            case ResourceTable.Id_btn_ov:
                slice = OrderedValidateSlice.class;
                break;
            case ResourceTable.Id_btn_ovt:
                slice = OrderedValidateTillSlice.class;
                break;
            case ResourceTable.Id_btn_qruio:
                slice = QuickRuleUnorderedInOrderedSlice.class;
                break;
            case ResourceTable.Id_btn_qru:
                slice = QuickRuleUnorderedSlice.class;
                break;
            case ResourceTable.Id_btn_qroc:
                slice = QuickRuleOnlyControllerSlice.class;
                break;
            case ResourceTable.Id_btn_qro:
                slice = QuickRuleOnlySlice.class;
                break;
            case ResourceTable.Id_btn_qrorder:
                slice = QuickRuleOrderedSlice.class;
                break;
            case ResourceTable.Id_btn_rr:
                slice = RemoveRulesSlice.class;
                break;
            case ResourceTable.Id_btn_us:
                slice = UnorderedSequencingSlice.class;
                break;
            case ResourceTable.Id_btn_cvda:
                slice = CustomViewDataAdapterSlice.class;
                break;
            case ResourceTable.Id_btn_uvb:
                slice = UnorderedValidateBeforeSlice.class;
                break;
            case ResourceTable.Id_btn_uv:
                slice = UnorderedValidateSlice.class;
                break;
            case ResourceTable.Id_btn_uvt:
                slice = UnorderedValidateTillSlice.class;
                break;
            case ResourceTable.Id_btn_viv:
            default:
                slice = ValidateInvisibleViewsSlice.class;
                break;
        }
        Intent intent = new Intent();
        intent.setOperation(new Intent.OperationBuilder().withAbilityName(slice).withBundleName(getBundleName()).build());
        startAbility(intent);
    }
}
