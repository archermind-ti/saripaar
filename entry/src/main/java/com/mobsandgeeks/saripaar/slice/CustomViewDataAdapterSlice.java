/*
 * Copyright (C) 2014 Mobs & Geeks
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mobsandgeeks.saripaar.slice;

import com.mobsandgeeks.saripaar.ResourceTable;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.adapter.ViewDataAdapter;
import com.mobsandgeeks.saripaar.annotation.AssertTrue;
import com.mobsandgeeks.saripaar.exception.ConversionException;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;

import java.lang.annotation.Annotation;
import java.util.List;

/**
 * @author Ragunath Jawahar {@literal <rj@mobsandgeeks.com>}
 */
public class CustomViewDataAdapterSlice extends Ability
        implements Validator.ValidationListener, Component.ClickedListener,
        AbsButton.CheckedStateChangedListener {

    @AssertTrue
    private StackLayout mBooleanFloatLabeledEditText;

    private RadioButton mRegisterAdapterRadioButton;
    private Text mResultTextView;
    private Button mSaripaarButton;

    // Attributes
    private Validator mValidator;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_slice_custom_view_data_adapter);

        // UI References
        mBooleanFloatLabeledEditText =
                (StackLayout) findComponentById(ResourceTable.Id_booleanFloatLabelEditText);
        mRegisterAdapterRadioButton = (RadioButton) findComponentById(ResourceTable.Id_registerAdapterRadioButton);
        mResultTextView = (Text) findComponentById(ResourceTable.Id_resultTextView);
        mSaripaarButton = (Button) findComponentById(ResourceTable.Id_saripaarButton);

        // Validator
        mValidator = new Validator(this);
        mValidator.setValidationListener(this);

        // Event listeners
        mSaripaarButton.setClickedListener(this);
        mRegisterAdapterRadioButton.setCheckedStateChangedListener(this);
    }

    @Override
    public void onValidationSucceeded() {
        mResultTextView.setText("success");
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        mResultTextView.setText(Common.getFailedFieldNames(errors));
    }

    @Override
    public void onClick(Component v) {
        try {
            mValidator.validate();
        } catch (UnsupportedOperationException e) {
            mResultTextView.setText("CRASH");
        }
    }

    @Override
    public void onCheckedChanged(AbsButton absButton, boolean b) {
        if (b) {
            mValidator.registerAdapter(StackLayout.class,
                    new ViewDataAdapter<StackLayout, Boolean>() {

                        @Override
                        public Boolean getData(StackLayout flet) throws ConversionException {
                            String booleanText = ((TextField) flet.findComponentById(ResourceTable.Id_booleanEditText)).getText().trim();
                            return Boolean.parseBoolean(booleanText);
                        }

                        @Override
                        public <T extends Annotation> boolean containsOptionalValue(
                                final StackLayout editText, final T ruleAnnotation) {
                            return false;
                        }
                    }
            );
        }
    }
}
