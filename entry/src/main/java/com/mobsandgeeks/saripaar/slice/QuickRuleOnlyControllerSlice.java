/*
 * Copyright (C) 2015 Mobs & Geeks
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mobsandgeeks.saripaar.slice;

import com.mobsandgeeks.saripaar.QuickRule;
import com.mobsandgeeks.saripaar.ResourceTable;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.app.Context;

import java.util.List;


public class QuickRuleOnlyControllerSlice extends Ability {

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);

        // Set content view

        LayoutScatter layoutInflater = LayoutScatter.getInstance(this);
        ComponentContainer view = (ComponentContainer) layoutInflater.parse(ResourceTable.Layout_slice_quick_rule_only, null, false);
//        setUIContent(view);
        setUIContent(view);
        // Controller
        new Controller(view);
    }

    static class Controller implements Component.ClickedListener, Validator.ValidationListener {
        // UI
        private final TextField mOneOnlyEditText;
        private final Text
         mResultTextView;
        private final Button mSaripaarButton;

        // Attributes
        private final Validator mValidator;

        Controller(final Component rootView) {
            // UI
            mOneOnlyEditText = (TextField) rootView.findComponentById(ResourceTable.Id_oneOnlyEditText);
            mResultTextView = (Text
        ) rootView.findComponentById(ResourceTable.Id_resultTextView);
            mSaripaarButton = (Button) rootView.findComponentById(ResourceTable.Id_saripaarButton);

            // Validation
            mValidator = new Validator(this);
            mValidator.setValidationListener(this);

            // Add a quick rule
            mValidator.put(mOneOnlyEditText, new QuickRule<Text
        >() {

                @Override
                public boolean isValid(Text Text) {
                    return "1".equals(Text.getText());
                }

                @Override
                public String getMessage(Context context) {
                    return "Enter 1, nothing else.";
                }
            });

            // Event listeners
            mSaripaarButton.setClickedListener(this);
        }

        @Override
        public void onClick(Component component) {
            try {
                mValidator.validate();
            } catch (IllegalStateException e) {
                mResultTextView.setText("crash");
            }
        }

        @Override
        public void onValidationSucceeded() {
            mResultTextView.setText("success");
        }

        @Override
        public void onValidationFailed(List<ValidationError> errors) {
            mResultTextView.setText("failure");
        }
    }
}
