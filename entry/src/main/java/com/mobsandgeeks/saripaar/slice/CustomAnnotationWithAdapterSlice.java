/*
 * Copyright (C) 2014 Mobs & Geeks
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mobsandgeeks.saripaar.slice;


import com.mobsandgeeks.saripaar.ResourceTable;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.adapter.ViewDataAdapter;
import com.mobsandgeeks.saripaar.exception.ConversionException;
import com.mobsandgeeks.saripaar.slice.validation.To;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;

import java.lang.annotation.Annotation;
import java.util.List;

/**
 * @author Ragunath Jawahar {@literal <rj@mobsandgeeks.com>}
 */
public class CustomAnnotationWithAdapterSlice extends Ability
        implements Validator.ValidationListener, Component.ClickedListener,
        AbsButton.CheckedStateChangedListener {

    @To(50)
    private ProgressBar mSeekBar;

    private RadioButton mRegisterAnnotationRadioButton;
    private Text
            mResultTextView;
    private Button mSaripaarButton;

    // Attributes
    private Validator mValidator;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_slice_custom_annotation_with_adapter);

        // UI References
        mSeekBar = (ProgressBar) findComponentById(ResourceTable.Id_ProgressBar);
        mRegisterAnnotationRadioButton =
                (RadioButton) findComponentById(ResourceTable.Id_registerAnnotationRadioButton);
        mResultTextView = (Text) findComponentById(ResourceTable.Id_resultTextView);
        mSaripaarButton = (Button) findComponentById(ResourceTable.Id_saripaarButton);

        // Validation
        mValidator = new Validator(this);
        mValidator.setValidationListener(this);

        // Event listeners
        mRegisterAnnotationRadioButton.setCheckedStateChangedListener(this);
        mSaripaarButton.setClickedListener(this);
    }

    @Override
    public void onClick(final Component v) {
        try {
            mValidator.validate();
        } catch (IllegalStateException e) {
            mResultTextView.setText("CRASH");
            e.printStackTrace();
        }
    }

    @Override
    public void onValidationSucceeded() {
        mResultTextView.setText("success");
    }

    @Override
    public void onValidationFailed(final List<ValidationError> errors) {
        mResultTextView.setText("failure");
    }

    @Override
    public void onCheckedChanged(final AbsButton buttonView, final boolean isChecked) {
        if (isChecked) {
            SeekBarIntegerAdapter seekBarIntegerAdapter = new SeekBarIntegerAdapter();
            Validator.registerAnnotation(To.class, ProgressBar.class, seekBarIntegerAdapter);
        }
    }

    static class SeekBarIntegerAdapter implements ViewDataAdapter<ProgressBar, Integer> {

        @Override
        public Integer getData(final ProgressBar ProgressBar) throws ConversionException {
            return ProgressBar.getProgress();
        }

        @Override
        public <T extends Annotation> boolean containsOptionalValue(
                final ProgressBar view, final T ruleAnnotation) {
            return false;
        }
    }

}
