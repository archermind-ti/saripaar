/*
 * Copyright (C) 2016 Mobs & Geeks
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mobsandgeeks.saripaar.slice;

import com.mobsandgeeks.saripaar.ResourceTable;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;

import java.util.List;

public class ValidateInvisibleViewsSlice extends Ability
        implements Component.ClickedListener, AbsButton.CheckedStateChangedListener,
                Validator.ValidationListener {

    // Fields
    @NotEmpty
    private TextField mNameEditText;

    @NotEmpty
    private TextField mEmailEditText;

    private Text
         mResultTextView;

    // Attributes
    private Validator mValidator;

     @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_slice_validate_invisible_views);

        // View references
        mNameEditText = (TextField) findComponentById(ResourceTable.Id_nameEditText);
        mEmailEditText = (TextField) findComponentById(ResourceTable.Id_emailEditText);
        mResultTextView = (Text) findComponentById(ResourceTable.Id_resultTextView);
        RadioButton showViewRadioButton = (RadioButton) findComponentById(ResourceTable.Id_showRadioButton);
        RadioButton hideViewRadioButton = (RadioButton) findComponentById(ResourceTable.Id_hideRadioButton);
        Checkbox validateHiddenViewsCheckBox =
                (Checkbox) findComponentById(ResourceTable.Id_validateHiddenViewsCheckBox);
        Button saripaarButton = (Button) findComponentById(ResourceTable.Id_saripaarButton);

        // Validator
        mValidator = new Validator(this);
        mValidator.setValidationListener(this);

        // Event listeners
        showViewRadioButton.setCheckedStateChangedListener(this);
        hideViewRadioButton.setCheckedStateChangedListener(this);
        validateHiddenViewsCheckBox.setCheckedStateChangedListener(this);
        saripaarButton.setClickedListener(this);
    }

    @Override
    public void onClick(Component view) {
        mValidator.validate();
    }

    @Override
    public void onCheckedChanged(AbsButton compoundButton, boolean checked) {
         switch (compoundButton.getId()) {
            case ResourceTable.Id_showRadioButton:
                if (checked)
                mEmailEditText.setVisibility(Component.VISIBLE);
                break;

            case ResourceTable.Id_hideRadioButton:
                if (checked)
                mEmailEditText.setVisibility(Component.INVISIBLE);
                break;

            case ResourceTable.Id_validateHiddenViewsCheckBox:
                mValidator.validateInvisibleViews(checked);
                break;
        }
    }

    @Override
    public void onValidationSucceeded() {
        mResultTextView.setText("success");
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        mResultTextView.setText("failure");
    }

}
