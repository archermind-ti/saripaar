/*
 * Copyright (C) 2014 Mobs & Geeks
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mobsandgeeks.saripaar.slice;

import com.mobsandgeeks.saripaar.HiLogUtils;
import com.mobsandgeeks.saripaar.ResourceTable;
import com.mobsandgeeks.saripaar.Rule;
import com.mobsandgeeks.saripaar.ValidationError;
import ohos.agp.components.Component;
import ohos.agp.components.StackLayout;
import ohos.agp.components.Text;

import java.util.List;

/**
 * @author Ragunath Jawahar {@literal <rj@mobsandgeeks.com>}
 */
public final class Common {

    public static String getFailedFieldNames(List<ValidationError> errors) {
        StringBuilder stringBuilder = new StringBuilder();
        for (ValidationError error : errors) {
            Component view = error.getView();
            Text text = view instanceof Text
                ? (Text) view
                    : null;
            if (text!=null) {
                StackLayout stackLayout = view instanceof StackLayout ? (StackLayout) view : null;
                if (stackLayout != null)
                    text = (ohos.agp.components.Text) stackLayout.findComponentById(ResourceTable.Id_booleanEditText);
            }
            if (text==null){
                return "text catch";
            }

            List<Rule> failedRules = error.getFailedRules();
            String fieldName = text.getHint().toUpperCase().replaceAll(" ", "_");

            for (Rule failedRule : failedRules) {
                stringBuilder.append(fieldName).append(" ");
                HiLogUtils.info(Rule.class.getSimpleName(), failedRule.toString());
            }
        }
        return stringBuilder.toString();
    }

}
