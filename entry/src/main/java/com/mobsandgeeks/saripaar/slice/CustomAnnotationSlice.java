/*
 * Copyright (C) 2014 Mobs & Geeks
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mobsandgeeks.saripaar.slice;


import com.mobsandgeeks.saripaar.ResourceTable;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;

import java.util.List;

/**
 * @author Ragunath Jawahar {@literal <rj@mobsandgeeks.com>}
 */
public class CustomAnnotationSlice extends Ability
        implements Validator.ValidationListener, Component.ClickedListener {

    @HometownZipCode
    private TextField mZipCodeEditText;

    private Text
            mResultTextView;
    private Button mSaripaarButton;

    // Attributes
    private Validator mValidator;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_slice_custom_annotation);

        // UI References
        mZipCodeEditText = (TextField) findComponentById(ResourceTable.Id_zipCodeEditText);
        RadioButton registerAnnotationRadioButton =
                (RadioButton) findComponentById(ResourceTable.Id_registerAnnotationRadioButton);
        mResultTextView = (Text) findComponentById(ResourceTable.Id_resultTextView);
        mSaripaarButton = (Button) findComponentById(ResourceTable.Id_saripaarButton);

        // Validator
        mValidator = new Validator(this);
        mValidator.setValidationListener(this);

        // Event listeners
        registerAnnotationRadioButton.setCheckedStateChangedListener(
                new AbsButton.CheckedStateChangedListener() {
                    @Override
                    public void onCheckedChanged(AbsButton buttonView, boolean isChecked) {
                        if (isChecked) {
                            Validator.registerAnnotation(HometownZipCode.class);
                        }
                    }
                }
        );
        mSaripaarButton.setClickedListener(this);
    }

    @Override
    public void onValidationSucceeded() {
        mResultTextView.setText("success");
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        mResultTextView.setText(Common.getFailedFieldNames(errors));
    }

    @Override
    public void onClick(Component component) {
        try {
            mValidator.validate();
        } catch (IllegalStateException e) {
            mResultTextView.setText("CRASH");
        }
    }

}
