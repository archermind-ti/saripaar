package com.mobsandgeeks.saripaar.slice;

import com.mobsandgeeks.saripaar.QuickRule;
import com.mobsandgeeks.saripaar.ResourceTable;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.app.Context;

import java.util.List;

/**
 * @author Ragunath Jawahar {@literal <rj@mobsandgeeks.com>}
 */
public class RemoveRulesSlice extends Ability
        implements Validator.ValidationListener, Component.ClickedListener {

    // Fields
    @Email
    private TextField mEmailEditText;

    private RadioButton mAddQuickRuleRadioButton;
    private RadioButton mRemoveRulesRadioButton;
    private Text
         mResultTextView;
    private Button mSaripaarButton;

    // Attributes
    private Validator mValidator;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_slice_remove_rules);

        // UI References
        mEmailEditText = (TextField) findComponentById(ResourceTable.Id_emailEditText);
        mAddQuickRuleRadioButton = (RadioButton) findComponentById(ResourceTable.Id_addQuickRuleRadioButton);
        mRemoveRulesRadioButton = (RadioButton) findComponentById(ResourceTable.Id_removeRulesRadioButton);
        mResultTextView = (Text) findComponentById(ResourceTable.Id_resultTextView);
        mSaripaarButton = (Button) findComponentById(ResourceTable.Id_saripaarButton);

        // Validator
        mValidator = new Validator(this);

        // Event listeners
        mSaripaarButton.setClickedListener(this);
        mValidator.setValidationListener(this);
        mAddQuickRuleRadioButton.setCheckedStateChangedListener(
                new AbsButton.CheckedStateChangedListener() {

                    @Override
                    public void onCheckedChanged(AbsButton buttonView, boolean isChecked) {
                        if (isChecked)
                            mValidator.put(mEmailEditText, new QuickRule<TextField>() {
                            @Override
                            public boolean isValid(TextField TextField) {
                                String email = TextField.getText();
                                return email.endsWith("mobsandgeeks.com");
                            }

                            @Override
                            public String getMessage(Context context) {
                                return "Only allow emails from \"mobsandgeeks.com\" :P";
                            }
                        });
                    }
                });

        mRemoveRulesRadioButton.setCheckedStateChangedListener(
                new AbsButton.CheckedStateChangedListener() {

                    @Override
                    public void onCheckedChanged(AbsButton buttonView, boolean isChecked) {
                        if (isChecked)
                            mValidator.removeRules(mEmailEditText);
                    }
                });
    }

    @Override
    public void onClick(Component component) {
        try {
            mValidator.validate();
        } catch (IllegalStateException e) {
            mResultTextView.setText("crash");
        }
    }

    @Override
    public void onValidationSucceeded() {
        mResultTextView.setText("success");
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        mResultTextView.setText(Common.getFailedFieldNames(errors));
    }
}
