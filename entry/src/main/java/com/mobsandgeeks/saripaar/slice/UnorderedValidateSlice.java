/*
 * Copyright (C) 2014 Mobs & Geeks
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file
 * except in compliance with the License. You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the
 * License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * either express or implied. See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.mobsandgeeks.saripaar.slice;

import com.mobsandgeeks.saripaar.ResourceTable;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.Length;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;

import java.util.List;

public class UnorderedValidateSlice extends Ability
        implements Validator.ValidationListener, RadioContainer.CheckedStateChangedListener {

    // Fields
    @NotEmpty
    private TextField mNameEditText;

    @NotEmpty
    private TextField mAddressEditText;

    @Email
    private TextField mEmailEditText;

    @Length(min = 10, max = 10)
    private TextField mPhoneEditText;

    private Text
         mResultTextView;

    // Attributes
    private Validator mValidator;

     @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_slice_un_ordered_validate);

        // UI References
        mNameEditText = (TextField) findComponentById(ResourceTable.Id_nameEditText);
        mAddressEditText = (TextField) findComponentById(ResourceTable.Id_addressEditText);
        mEmailEditText = (TextField) findComponentById(ResourceTable.Id_emailEditText);
        mPhoneEditText = (TextField) findComponentById(ResourceTable.Id_phoneEditText);
        mResultTextView = (Text) findComponentById(ResourceTable.Id_resultTextView);
        RadioContainer modeRadioGroup = (RadioContainer) findComponentById(ResourceTable.Id_modeRadioGroup);
        Button saripaarButton = (Button) findComponentById(ResourceTable.Id_saripaarButton);

        // Validator
        mValidator = new Validator(this);
        mValidator.setValidationListener(this);

        // Event listeners
        modeRadioGroup.setMarkChangedListener(this);
        saripaarButton.setClickedListener(new Component.ClickedListener() {

            @Override
            public void onClick(Component component) {
                try {
                    mValidator.validate();
                } catch (IllegalStateException e) {
                    mResultTextView.setText("CRASH");
                }
            }
        });
    }

    @Override
    public void onValidationSucceeded() {
        mResultTextView.setText("success");
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        mResultTextView.setText(Common.getFailedFieldNames(errors));
    }

    @Override
    public void onCheckedChanged(RadioContainer group, int checkedId) {
        switch (checkedId) {
            case 0:
                mValidator.setValidationMode(Validator.Mode.BURST);
                break;

            case 1:
                mValidator.setValidationMode(Validator.Mode.IMMEDIATE);
                break;
        }
    }
}
